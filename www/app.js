import angular from 'angular';
import 'angular-ui-router';

import './assets/main.scss';

import HomeIndexController from './components/home/indexController';

angular.element(document).ready(function () {
    angular.bootstrap(document.body, ['app']);
});

export default angular.module('app', ['ui.router'])
    .config(($locationProvider, $stateProvider, $urlRouterProvider) => {

        $urlRouterProvider.otherwise('/');

        $stateProvider
        .state('home', {
            url: '/',
            controller: HomeIndexController,
            template: require('./components/home/indexView.html')
        })
        ;
    })
    ;
